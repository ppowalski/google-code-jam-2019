#include <iostream>
#include <string>

int main()
{
    std::ios::sync_with_stdio(false);

    unsigned int test;
    unsigned int size;
    std::string linda_path;

    std::cin >> test;
    for(unsigned int i=0; i<test; i++)
    {
        std::cin >> size;
        std::cin >> linda_path;

        std::cout << "Case #" << i+1 << ": ";
        for(unsigned int j=0; j<linda_path.size(); j++)
        {
            if(linda_path[j] == 'S')
            {
                std::cout << "E";
            }
            else
            {
                std::cout << "S";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}