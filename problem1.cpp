#include <iostream>
#include <string>

int main()
{
    std::ios::sync_with_stdio(false);
    
    unsigned int test;
    std::string s_number;
    std::string num1 = "";
    std::string num2 = "";
    bool zero = false;

    std::cin >> test;
    for(unsigned int i=0; i<test; i++)
    {
        num1 = "";
        num2 = "";
        std::cin >> s_number;
        for(unsigned int j=0; j<s_number.size(); j++)
        {
            if(s_number[j] == '4')
            {
                num1 += '2';
                num2 += '2';
                zero = true;
            }
            else
            {
                num1 += s_number[j];
                if(zero)
                {
                    num2 += '0';
                }
            }
        }

        std::cout << "Case #" << i+1 <<": " << num1 << " " << num2 << std::endl;
    }

    return 0;
}