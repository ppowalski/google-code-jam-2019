#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

struct num
{
    unsigned int number;
    unsigned int key;
};

std::vector<unsigned int> prime(unsigned int max)
{
    std::vector<unsigned int> tab;
    unsigned int now_p = 2;
    bool is_prime = true;

    while(now_p <= max)
    {
        is_prime = true;
        for(unsigned int i = 2; i <= (int)sqrt(now_p); i++)
        {
            if(now_p%i == 0)
            {
                is_prime = false;
                break;
            }
        }

        if(is_prime)
        {
            tab.push_back(now_p);
        }

        now_p++;
    }

    return tab;
}

int main()
{
    std::ios::sync_with_stdio(false);

    unsigned int test;
    unsigned int prime_number;
    unsigned int length;
    unsigned int number;

    std::cin >> test;
    for(unsigned int i=0; i<test; i++)
    {
        if(i!=0) std::cout << std::endl;
        
        std::set<unsigned int> str;

        std::cin >> prime_number >> length;
        num tab[length+1];
        std::vector<unsigned int> tab_prime = prime(prime_number);
        unsigned int last = 0;
        unsigned int val1 = 0;
        unsigned int val2 = 0;

        for(unsigned int j=0; j<length; j++)
        {
            std::cin >> number;
            tab[j].number = number;

            for(unsigned int k=0; k<tab_prime.size(); k++)
            {
                if(number%tab_prime[k] == 0)
                {
                    if(last == 0)
                    {
                        val1 = number/tab_prime[k];
                        val2 = tab_prime[k];
                        last = 1;
                        break;
                    }
                    else if(last == 1)
                    {
                        if((number/tab_prime[k]) == val1 || tab_prime[k] == val1)
                        {
                            tab[j-1].key = val2;
                            tab[j].key = val1;
                            last = val1;
                        }
                        else
                        {
                            tab[j-1].key = val1;
                            tab[j].key = val2;
                            last = val2;
                        }

                        str.insert(val1);
                        str.insert(val2);

                        val1 = number/tab_prime[k];
                        val2 = tab_prime[k];
                        break;
                    }
                    else
                    {
                        if(val1 == last)
                        {
                            tab[j].key = val2;
                            last = val2;
                        }
                        else
                        {
                            tab[j].key = val1;
                            last = val1;
                        }

                        val1 = number/tab_prime[k];
                        val2 = tab_prime[k];

                        str.insert(last);

                        if(length-1 == j)
                        {
                            str.insert(val1);
                            str.insert(val2);

                            if(val1 == last)
                            {
                                tab[j+1].key = val2;
                                last = val2;
                            }
                            else
                            {
                                tab[j+1].key = val1;
                                last = val1;
                            }
                        }
                        break;
                    }
                }
            }
        }

        std::map<unsigned int, char> letters;
        unsigned int chr = 65;
        for(auto it = str.begin(); it != str.end(); it++)
        {
            letters[*it] = chr;
            chr++;
        }

        std::cout << "Case #" << i+1 << ": ";
        for(int j=0; j<length+1; j++)
        {
            std::cout << letters[tab[j].key];
        }
    }

    return 0;
}